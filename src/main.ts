import { NestFactory } from '@nestjs/core';
import { Logger } from '@nestjs/common';
import { AppModule } from './app.module';
import helmet from 'helmet';
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as dotenv from 'dotenv';
import { Sequelize } from 'sequelize';
import { configDB } from './config/config-env';
import { Category } from './modules/categories/models/entity/categoty.entity';
dotenv.config({
  path: '.env',
});

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.use(
    cors({
      origin: true,
      methods: ['POST', 'PUT', 'OPTIONS', 'DELETE', 'GET', 'PATCH'],
      allowedHeaders: [
        'authorization',
        'Origin',
        'Access-Control-Allow-Origin',
        'Access-Control-Request-Headers',
        'Cache-Control',
        'skip',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'user-id',
        'x-request-public-key',
        'Content-Length',
      ],
      credentials: false,
    }),
  );
  app.use(helmet({ frameguard: false }));
  app.use(bodyParser.json({ limit: '50mb', type: 'application/json' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  const port = process.env.PORT || 3000;
  app.use((req, res, next) => {
    console.log(req.method.toUpperCase(), req.originalUrl);
    return next();
  });
  await app.listen(port);
  Logger.log(
    `🚀 Application is running on: http://localhost:${port}/${globalPrefix}`,
  );
}
bootstrap();
