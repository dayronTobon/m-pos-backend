import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

@Injectable()
export class BcryptService {
  saltRounds = 10;
  encrypt(text: string) {
    const textEncrypt = bcrypt.hashSync(text, this.saltRounds);
    return textEncrypt;
  }
  compare(pass1, pass2) {
    return bcrypt.compareSync(pass1, pass2);
  }
}
