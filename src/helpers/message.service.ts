import { Injectable } from '@nestjs/common';

@Injectable()
export class MessageService {
  messageOk(code = 200, data: any) {
    return {
      status: true,
      code,
      message: 'Ok',
      data,
    };
  }

  messageError(code = 500, error: any) {
    return {
      status: false,
      code,
      message: 'Error',
      error,
    };
  }
}
