import { Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';

@Injectable()
export class TokenService {
  getToken(user: any) {
    const TOKEN_SECRET_KEY = process.env.TOKEN_SECRET_KEY;
    if (TOKEN_SECRET_KEY) {
      const token = jwt.sign(
        {
          data: user,
        },
        TOKEN_SECRET_KEY,
        {
          expiresIn: process.env.EXPIRES_IN
            ? parseInt(process.env.EXPIRES_IN)
            : 604800,
        },
      );
      return token;
    } else {
      return TOKEN_SECRET_KEY;
    }
  }
}
