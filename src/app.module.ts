import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { databaseProviders } from './config/database.providers';
import { AuthModule } from './modules/auth/auth.module';
import { CategoriesModule } from './modules/categories/categories.module';
import { ProductsModule } from './modules/products/products.module';
import { UsersModule } from './modules/users/users.module';
import { MessageService } from './helpers/message.service';
import { TokenService } from './helpers/token.service';
import { BcryptService } from './helpers/bcrypt.service';
import { AuthMiddleware } from './middlewares/auth.middleware';

@Module({
  imports: [AuthModule, CategoriesModule, ProductsModule, UsersModule],
  controllers: [AppController],
  providers: [
    AppService,
    ...databaseProviders,
    MessageService,
    TokenService,
    BcryptService,
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: 'api/user', method: RequestMethod.POST },
        { path: 'api/auth', method: RequestMethod.POST },
      )
      .forRoutes('');
  }
}
