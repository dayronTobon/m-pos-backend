require('dotenv').config();

module.exports = {
  development: {
    dialect: process.env.DB_DIALECT || 'mssql',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 1433,
    username: process.env.DB_USERNAME || 'root',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || 'mpos',
    dialectOptions: {
      bigNumberStrings: true,
    },
  },
  test: {
    dialect: process.env.DB_DIALECT || 'mssql',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 1433,
    username: process.env.DB_USERNAME || 'root',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || 'mpos',
    dialectOptions: {
      bigNumberStrings: true,
    },
  },
  production: {
    dialect: process.env.DB_DIALECT || 'mssql',
    host: process.env.DB_HOST || 'localhost',
    port: parseInt(process.env.DB_PORT) || 1433,
    username: process.env.DB_USERNAME || 'root',
    password: process.env.DB_PASSWORD || '',
    database: process.env.DB_DATABASE || 'mpos',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: {
      //   ca: fs.readFileSync(__dirname + '/mssql-ca-main.crt'),
      // },
    },
  },
};
