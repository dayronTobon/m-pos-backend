import { Sequelize } from 'sequelize-typescript';
import { configDB } from './config-env';
import { Category } from '../modules/categories/models/entity/categoty.entity';
import { Product } from '../modules/products/models/entity/product.entity';
import { User } from '../modules/users/models/entity/user.entity';
import { ListTags } from '../modules/products/models/entity/list-tags.entity';
import * as dotenv from 'dotenv';
dotenv.config();

export const databaseProviders = [
  {
    provide: 'SEQUELIZE',
    useFactory: async () => {
      const enviroment = process.env.NODE_ENV || 'development';
      const configSelect = configDB[enviroment];
      const sequelize = new Sequelize(configSelect);
      sequelize.addModels([User, Category, Product, ListTags]);
      await sequelize.sync();
      return sequelize;
    },
  },
];
