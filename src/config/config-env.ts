import * as dotenv from 'dotenv';
import { Dialect } from 'sequelize';
dotenv.config();

export const configDB = {
  development: {
    dialect: (process.env.DB_DIALECT as Dialect) || 'mssql',
    host: (process.env.DB_HOST as string) || 'localhost',
    port: (parseInt(process.env.DB_PORT) as number) || 1433,
    username: (process.env.DB_USERNAME as string) || 'root',
    password: (process.env.DB_PASSWORD as string) || '',
    database: (process.env.DB_DATABASE as string) || 'mpos',
    dialectOptions: {
      bigNumberStrings: true,
    },
  },
  test: {
    dialect: (process.env.DB_DIALECT as Dialect) || 'mssql',
    host: (process.env.DB_HOST as string) || 'localhost',
    port: (parseInt(process.env.DB_PORT) as number) || 1433,
    username: (process.env.DB_USERNAME as string) || 'root',
    password: (process.env.DB_PASSWORD as string) || '',
    database: (process.env.DB_DATABASE as string) || 'mpos',
    dialectOptions: {
      bigNumberStrings: true,
    },
  },
  production: {
    dialect: (process.env.DB_DIALECT as Dialect) || 'mssql',
    host: (process.env.DB_HOST as string) || 'localhost',
    port: (parseInt(process.env.DB_PORT) as number) || 1433,
    username: (process.env.DB_USERNAME as string) || 'root',
    password: (process.env.DB_PASSWORD as string) || '',
    database: (process.env.DB_DATABASE as string) || 'mpos',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: {
      //   ca: fs.readFileSync(__dirname + '/mssql-ca-main.crt'),
      // },
    },
  },
};
