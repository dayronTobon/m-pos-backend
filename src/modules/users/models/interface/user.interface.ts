export interface IUser {
  full_name: string;
  username: string;
  password: string;
  rol?: 'ADMIN' | 'USER';
  last_entry_date?: Date;
  status?: boolean;
  permanently_disable?: boolean;
}
