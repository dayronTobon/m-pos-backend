import { Table, Column, Model, DataType } from 'sequelize-typescript';
import { IUser } from '../interface/user.interface';

@Table({
  tableName: 'users',
})
export class User extends Model implements IUser {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
  })
  id: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  full_name: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
    unique: true,
  })
  username: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  password: string;

  @Column({
    type: DataType.ENUM,
    values: ['ADMIN', 'USER'],
    allowNull: false,
    defaultValue: 'USER',
  })
  rol?: 'ADMIN' | 'USER';

  @Column({
    type: DataType.DATE,
  })
  last_entry_date?: Date;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  status?: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  permanently_disable?: boolean;
}
