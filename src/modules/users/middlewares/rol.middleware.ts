import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { MessageService } from 'src/helpers/message.service';
import * as jwt from 'jsonwebtoken';
import { DecodeOptions } from 'jsonwebtoken';

@Injectable()
export class RolMiddleware implements NestMiddleware {
  constructor(private messageService: MessageService) {}

  use(req: any, res: any, next: () => void) {
    const API_SECRET = process.env.TOKEN_SECRET_KEY;
    jwt.verify(
      req.headers['authorization'].replace('Bearer ', ''),
      API_SECRET,
      async (err: Error, decoded: DecodeOptions) => {
        if (err) {
          const errorCreate = await this.messageService.messageOk(
            HttpStatus.UNAUTHORIZED,
            'unauthorized user',
          );
          res.status(HttpStatus.UNAUTHORIZED).json(errorCreate);
          return;
        }
        const dataUser = decoded.data;
        if (dataUser.rol != 'ADMIN') {
          const errorCreate = await this.messageService.messageOk(
            HttpStatus.FORBIDDEN,
            'unauthorized user',
          );
          res.status(HttpStatus.FORBIDDEN).json(errorCreate);
          return;
        }
        next();
      },
    );
  }
}
