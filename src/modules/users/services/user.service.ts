import { Inject, Injectable } from '@nestjs/common';
import { User } from '../models/entity/user.entity';
import { IUser } from '../models/interface/user.interface';
import { IAuth } from '../../auth/models/auth.interface';
import { configColumnsUser } from './user-columns';
import { BcryptService } from '../../../helpers/bcrypt.service';

@Injectable()
export class UserService {
  constructor(
    @Inject('USER_REPOSITORY') private userRepository: typeof User,
    private bcryptService: BcryptService,
  ) {}

  async getListUsers() {
    const listUsers = await this.userRepository.findAll<User>({
      attributes: { exclude: ['password'] },
      where: { permanently_disable: false },
      logging: false,
    });
    const res = {
      list: listUsers,
      configColumns: configColumnsUser,
    };
    return res;
  }

  async getUser(id: string) {
    return await this.userRepository.findByPk<User>(id, {
      attributes: { exclude: ['password'] },
    });
  }

  async getByUserName(user: IAuth) {
    return await this.userRepository.findOne<User>({
      where: { username: user.username },
    });
  }

  async createUser(userData: any) {
    userData.password = this.bcryptService.encrypt(userData.password);
    const dataInsert = await this.userRepository.build(userData);
    const user = await this.userRepository.create(dataInsert.dataValues, {
      logging: false,
    });
    return user;
  }

  async updateUser(id, userData) {
    const userUpdate = await this.userRepository.update(userData, {
      where: {
        id,
      },
    });
    return userUpdate;
  }

  async changeStatusUser(id, status: boolean) {
    const userUpdate = await this.userRepository.update(
      { status },
      {
        where: {
          id,
        },
      },
    );
    return userUpdate;
  }

  async deletesUser(id) {
    const userUpdate = await this.userRepository.update(
      { permanently_disable: true },
      {
        where: {
          id,
        },
      },
    );
    return userUpdate;
  }
}
