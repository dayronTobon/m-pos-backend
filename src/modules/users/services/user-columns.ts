export const configColumnsUser = [
  {
    type: 'text',
    key: 'full_name',
    columnName: 'Nombre completo',
  },
  {
    type: 'text',
    key: 'username',
    columnName: 'Nombre de usuario',
  },
  {
    type: 'text',
    key: 'rol',
    columnName: 'Rol',
  },
  {
    type: 'text',
    key: 'status',
    columnName: 'Estado',
    pipeCustom: {
      true: 'Activo',
      false: 'Inactivo',
    },
  },
  {
    type: 'buttons-actions',
    key: 'actions',
    columnName: 'Acciones',
    options: [
      {
        name: 'Desactivar',
        action: 'deactivate',
        icon: 'toggle_on',
        view: [
          {
            field: 'status',
            value: true,
          },
        ],
      },
      {
        name: 'Activar',
        action: 'active',
        icon: 'toggle_off',
        view: [
          {
            field: 'status',
            value: false,
          },
        ],
      },
      {
        name: 'Editar',
        action: 'edit',
        icon: 'edit',
        view: [
          {
            field: 'status',
            value: true,
          },
        ],
      },
      {
        name: 'Borrar',
        action: 'delete',
        icon: 'delete',
        view: [
          {
            field: 'status',
            value: false,
          },
        ],
      },
    ],
  },
];
