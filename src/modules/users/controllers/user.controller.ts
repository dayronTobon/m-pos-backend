import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { UserService } from '../services/user.service';
import { MessageService } from 'src/helpers/message.service';
import { IUser } from '../models/interface/user.interface';

@Controller('user')
export class UserController {
  constructor(
    private userService: UserService,
    private messageService: MessageService,
  ) {}

  @Get()
  async getListUsers(@Res() res) {
    try {
      const listUsers = await this.userService.getListUsers();
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, listUsers);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Get('/:id')
  async getUser(@Param('id') id, @Res() res) {
    try {
      const user = await this.userService.getUser(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, user);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Post()
  async createUser(@Body() user: IUser, @Res() res) {
    try {
      const newUser = await this.userService.createUser(user);
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, newUser);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Put('/:id')
  async updateUser(@Body() user: IUser, @Param('id') id, @Res() res) {
    try {
      const updateUser = await this.userService.updateUser(id, user);
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, updateUser);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Patch('/status/:id')
  async changeStatusUser(@Body() user: IUser, @Param('id') id, @Res() res) {
    try {
      const newStatusUser = await this.userService.changeStatusUser(
        id,
        user.status,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, newStatusUser);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Delete('/:id')
  async deleteUser(@Param('id') id, @Res() res) {
    try {
      const deleteUser = await this.userService.deletesUser(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, deleteUser);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }
}
