import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { userProviders } from './providers/user.provider';
import { UserController } from './controllers/user.controller';
import { UserService } from './services/user.service';
import { MessageService } from '../../helpers/message.service';
import { BcryptService } from '../../helpers/bcrypt.service';
import { RolMiddleware } from './middlewares/rol.middleware';

@Module({
  controllers: [UserController],
  providers: [...userProviders, UserService, MessageService, BcryptService],
})
export class UsersModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(RolMiddleware)
      .exclude({ path: 'api/user', method: RequestMethod.POST })
      .forRoutes('user');
  }
}
