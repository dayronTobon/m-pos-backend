import { Inject, Injectable } from '@nestjs/common';
import { Category } from '../models/entity/categoty.entity';
import { configColumnsCategory } from './category-columns';
import { Product } from 'src/modules/products/models/entity/product.entity';

@Injectable()
export class CategoryService {
  constructor(
    @Inject('CATEGORY_REPOSITORY') private categoryRepository: typeof Category,
  ) {}

  async getListCategories() {
    // const [listCategories, count] = await this.categoryRepository.sequelize
    //   .query(`
    //     SELECT
    //       category.id,
    //       category.name,
    //       category.status,
    //       category.permanently_disable,
    //       category.createdAt,
    //       category.updatedAt,
    //       product.id AS productId,
    //       product.name AS productName,
    //       product.price AS productPrice,
    //       product.cost AS productCost
    //       FROM categories AS category
    //     LEFT OUTER JOIN
    //       Products AS product
    //       ON category.id = products.id_category
    //     WHERE category.permanently_disable = 0;
    //   `);
    this.categoryRepository.associate(Product, 'product');
    const listCategories = await this.categoryRepository.findAll<Category>({
      include: {
        model: Product,
        attributes: ['id', 'name', 'price', 'cost'],
      },
      where: { permanently_disable: false },
      logging: false,
    });
    const res = {
      list: listCategories,
      configColumns: configColumnsCategory,
    };
    return res;
  }

  async getCategory(id: string) {
    return await this.categoryRepository.findByPk<Category>(id);
  }

  async createCategory(categoryData: any) {
    const dataInsert = await this.categoryRepository.build(categoryData);
    const category = await this.categoryRepository.create(
      dataInsert.dataValues,
      { logging: false },
    );
    return category;
  }

  async updateCategory(id, CategoryData) {
    const categoryUpdate = await this.categoryRepository.update(CategoryData, {
      where: {
        id,
      },
    });
    return categoryUpdate;
  }

  async changeStatusCategory(id, status: boolean) {
    const categoryUpdate = await this.categoryRepository.update(
      { status },
      {
        where: {
          id,
        },
      },
    );
    return categoryUpdate;
  }

  async deletesCategory(id) {
    const categoryDelete = await this.categoryRepository.update(
      { permanently_disable: true },
      {
        where: {
          id,
        },
      },
    );
    return categoryDelete;
  }
}
