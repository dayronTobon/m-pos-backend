import { Module } from '@nestjs/common';
import { categoryProviders } from './providers/category.provider';
import { CategoryService } from './services/category.service';
import { CategoryController } from './controllers/category.controller';
import { MessageService } from '../../helpers/message.service';

@Module({
  providers: [...categoryProviders, CategoryService, MessageService],
  controllers: [CategoryController],
})
export class CategoriesModule {}
