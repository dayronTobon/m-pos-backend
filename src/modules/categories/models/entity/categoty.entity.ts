import {
  Table,
  Column,
  Model,
  HasOne,
  DataType,
  Sequelize,
  HasMany,
} from 'sequelize-typescript';
import { ICategory } from '../interface/category.interface';
import { Product } from '../../../products/models/entity/product.entity';
import { IProduct } from '../../../products/models/interface/product.interface';
import { configDB } from 'src/config/config-env';
import { DataTypes } from 'sequelize';

@Table({
  tableName: 'categories',
})
export class Category extends Model implements ICategory {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
  })
  id: string;

  @Column({
    unique: true,
  })
  name: string;

  @HasMany(() => Product)
  products: IProduct[];

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  status?: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  permanently_disable?: boolean;

  static associate(models, nameModel) {
    // define association here
    switch (nameModel.toUpperCase()) {
      case 'PRODUCT':
        Category.hasMany(models, { foreignKey: 'id_category' });
        break;
      default:
        break;
    }
  }
}