export interface ICategory {
  name: string;
  status?: boolean;
  permanently_disable?: boolean;
}
