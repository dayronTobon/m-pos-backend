import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { MessageService } from '../../../helpers/message.service';
import { CategoryService } from '../services/category.service';
import { ICategory } from '../models/interface/category.interface';

@Controller('category')
export class CategoryController {
  constructor(
    private categoryService: CategoryService,
    private messageService: MessageService,
  ) {}

  @Get()
  async getListCategorys(@Res() res) {
    try {
      const listCategories = await this.categoryService.getListCategories();
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, listCategories);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Get('/:id')
  async getCategory(@Param('id') id, @Res() res) {
    try {
      const category = await this.categoryService.getCategory(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, category);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Post()
  async createCategory(@Body() category: ICategory, @Res() res) {
    try {
      const newCategory = await this.categoryService.createCategory(category);
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, newCategory);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Put('/:id')
  async updateCategory(
    @Body() category: ICategory,
    @Param('id') id,
    @Res() res,
  ) {
    try {
      const updateCategory = await this.categoryService.updateCategory(
        id,
        category,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, updateCategory);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Patch('/status/:id')
  async changeStatusCategory(
    @Body() category: ICategory,
    @Param('id') id,
    @Res() res,
  ) {
    try {
      const newStatusCategory = await this.categoryService.changeStatusCategory(
        id,
        category.status,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(
        code,
        newStatusCategory,
      );
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Delete('/:id')
  async deleteCategory(@Param('id') id, @Res() res) {
    try {
      const deleteCategory = await this.categoryService.deletesCategory(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, deleteCategory);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }
}
