import { Injectable } from '@nestjs/common';
import { IAuth } from '../models/auth.interface';
import { UserService } from '../../users/services/user.service';
import { BcryptService } from '../../../helpers/bcrypt.service';
import { TokenService } from '../../../helpers/token.service';
import { menuAdmin, menuUser } from './module-access';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private bcryptService: BcryptService,
    private tokenService: TokenService,
  ) {}

  async authentication(user: IAuth) {
    const userExist = await this.userService.getByUserName(user);
    if (!userExist) {
      return {
        status: false,
        message: 'invalid username or password',
      };
    }
    const equal = await this.bcryptService.compare(
      user.password,
      userExist.password,
    );
    if (!equal) {
      return {
        status: false,
        message: 'invalid username or password',
      };
    }
    const token = await this.tokenService.getToken(userExist);
    if (!token || token instanceof Error) {
      return {
        status: false,
        message: 'error generating token',
      };
    }
    return {
      status: true,
      token: token,
      user: userExist,
      menuAccess: userExist.rol == 'ADMIN' ? menuAdmin : menuUser,
      expiresIn: parseInt(process.env.EXPIRES_IN),
      rol: userExist.rol,
    };
  }
}
