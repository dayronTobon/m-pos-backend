export const menuAdmin = [
  {
    name: 'Usuarios',
    icon: 'group',
    path: 'user',
  },
  {
    name: 'Categorías',
    icon: 'category',
    path: 'category',
  },
  {
    name: 'Productos',
    icon: 'shopping_basket',
    path: 'product',
  },
  {
    name: 'Detalles de producto',
    icon: 'format_list_bulleted',
    path: 'list-tags',
  },
];

export const menuUser = [
  {
    name: 'Categorías',
    icon: 'category',
    path: 'category',
  },
  {
    name: 'Productos',
    icon: 'shopping_basket',
    path: 'product',
  },
  {
    name: 'Detalles de producto',
    icon: 'format_list_bulleted',
    path: 'list-tags',
  },
];
