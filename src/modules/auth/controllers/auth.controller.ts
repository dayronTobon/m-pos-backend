import { Body, Controller, HttpStatus, Post, Req, Res } from '@nestjs/common';
import { MessageService } from 'src/helpers/message.service';
import { IAuth } from '../models/auth.interface';
import { AuthService } from '../services/auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private authService: AuthService,
    private messageService: MessageService,
  ) {}

  @Post()
  async userAuthentication(@Body() user: IAuth, @Res() res) {
    try {
      const userAuthenticated = await this.authService.authentication(user);
      if (!userAuthenticated.status) {
        const code = HttpStatus.UNAUTHORIZED;
        const responseData = this.messageService.messageOk(
          code,
          userAuthenticated,
        );
        return res.status(code).json(responseData);
      }
      const code = HttpStatus.OK;
      const responseData = this.messageService.messageOk(
        code,
        userAuthenticated,
      );
      return res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }
}
