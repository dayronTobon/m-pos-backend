import { Module } from '@nestjs/common';
import { AuthController } from './controllers/auth.controller';
import { AuthService } from './services/auth.service';
import { UserService } from '../users/services/user.service';
import { MessageService } from '../../helpers/message.service';
import { TokenService } from '../../helpers/token.service';
import { BcryptService } from '../../helpers/bcrypt.service';
import { userProviders } from '../users/providers/user.provider';

@Module({
  controllers: [AuthController],
  providers: [
    ...userProviders,
    AuthService,
    UserService,
    MessageService,
    TokenService,
    BcryptService,
  ],
})
export class AuthModule {}
