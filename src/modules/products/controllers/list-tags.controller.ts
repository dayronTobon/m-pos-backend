import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { MessageService } from '../../../helpers/message.service';
import { ListTagsService } from '../services/list-tags.service';
import { IListTags } from '../models/interface/list-tags.interface';

@Controller('list-tags')
export class ListTagsController {
  constructor(
    private listTagsService: ListTagsService,
    private messageService: MessageService,
  ) {}

  @Get()
  async getListListTags(@Res() res) {
    try {
      const allListTags = await this.listTagsService.allListTags();
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, allListTags);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Get('/:id')
  async getListTags(@Param('id') id, @Res() res) {
    try {
      const listTags = await this.listTagsService.getListTags(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, listTags);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Post()
  async createListTags(@Body() ListTags: IListTags, @Res() res) {
    try {
      const newListTags = await this.listTagsService.createListTags(ListTags);
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, newListTags);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Put('/:id')
  async updateListTags(
    @Body() listTags: IListTags,
    @Param('id') id,
    @Res() res,
  ) {
    try {
      const updateListTags = await this.listTagsService.updateListTags(
        id,
        listTags,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, updateListTags);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Patch('/status/:id')
  async changeStatusListTags(
    @Body() listTags: IListTags,
    @Param('id') id,
    @Res() res,
  ) {
    try {
      const newStatusListTags = await this.listTagsService.changeStatusListTags(
        id,
        listTags.status,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(
        code,
        newStatusListTags,
      );
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Delete('/:id')
  async deleteListTags(@Param('id') id, @Res() res) {
    try {
      const deleteListTags = await this.listTagsService.deletesListTags(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, deleteListTags);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }
}
