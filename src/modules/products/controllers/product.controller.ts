import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import { MessageService } from '../../../helpers/message.service';
import { ProductService } from '../services/product.service';
import { IProduct } from '../models/interface/product.interface';

@Controller('product')
export class ProductController {
  constructor(
    private productService: ProductService,
    private messageService: MessageService,
  ) {}

  @Get()
  async getListProducts(@Res() res) {
    try {
      const listProducts = await this.productService.getListProducts();
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, listProducts);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Get('/:id')
  async getProduct(@Param('id') id, @Res() res) {
    try {
      const product = await this.productService.getProduct(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, product);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Post()
  async createProduct(@Body() product: IProduct, @Res() res) {
    try {
      const newProduct = await this.productService.createProduct(product);
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, newProduct);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Put('/:id')
  async updateProduct(@Body() product: IProduct, @Param('id') id, @Res() res) {
    try {
      const updateProduct = await this.productService.updateProduct(
        id,
        product,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(code, updateProduct);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Patch('/status/:id')
  async changeStatusProduct(
    @Body() product: IProduct,
    @Param('id') id,
    @Res() res,
  ) {
    try {
      const newStatusProduct = await this.productService.changeStatusProduct(
        id,
        product.status,
      );
      const code = HttpStatus.CREATED;
      const responseData = this.messageService.messageOk(
        code,
        newStatusProduct,
      );
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }

  @Delete('/:id')
  async deleteProduct(@Param('id') id, @Res() res) {
    try {
      const deleteProduct = await this.productService.deletesProduct(id);
      const code = HttpStatus.ACCEPTED;
      const responseData = this.messageService.messageOk(code, deleteProduct);
      res.status(code).json(responseData);
    } catch (error) {
      const code = HttpStatus.INTERNAL_SERVER_ERROR;
      const responseError = this.messageService.messageError(code, error);
      res.status(code).json(responseError);
    }
  }
}
