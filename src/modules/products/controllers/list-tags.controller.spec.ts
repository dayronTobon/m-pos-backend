import { Test, TestingModule } from '@nestjs/testing';
import { ListTagsController } from './list-tags.controller';

describe('ListTagsController', () => {
  let controller: ListTagsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ListTagsController],
    }).compile();

    controller = module.get<ListTagsController>(ListTagsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
