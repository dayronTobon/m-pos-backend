import { Test, TestingModule } from '@nestjs/testing';
import { ListTagsService } from './list-tags.service';

describe('ListTagsService', () => {
  let service: ListTagsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ListTagsService],
    }).compile();

    service = module.get<ListTagsService>(ListTagsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
