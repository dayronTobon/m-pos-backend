import { Inject, Injectable } from '@nestjs/common';
import { ListTags } from '../models/entity/list-tags.entity';
import { Product } from '../models/entity/product.entity';
import { configColumnsListTags } from './list-tags-columns';

@Injectable()
export class ListTagsService {
  constructor(
    @Inject('LIST_TAGS_REPOSITORY') private listTagsRepository: typeof ListTags,
  ) {}

  async allListTags() {
    this.listTagsRepository.associate(Product, 'product');
    const alllistTags = await this.listTagsRepository.findAll<ListTags>({
      include: [
        {
          model: Product,
          attributes: ['id', 'name'],
        },
      ],
      where: { permanently_disable: false },
      logging: false,
    });
    const res = {
      list: alllistTags,
      configColumns: configColumnsListTags,
    };
    return res;
  }

  async getListTags(id: string) {
    return await this.listTagsRepository.findByPk<ListTags>(id);
  }

  async createListTags(ListTagsData: any) {
    const dataInsert = await this.listTagsRepository.build(ListTagsData);
    const listTags = await this.listTagsRepository.create(
      dataInsert.dataValues,
      {
        logging: false,
      },
    );
    return listTags;
  }

  async updateListTags(id, ListTagsData) {
    const listTagsUpdate = await this.listTagsRepository.update(ListTagsData, {
      where: {
        id,
      },
    });
    return listTagsUpdate;
  }

  async changeStatusListTags(id, status: boolean) {
    const listTagsUpdate = await this.listTagsRepository.update(
      { status },
      {
        where: {
          id,
        },
      },
    );
    return listTagsUpdate;
  }

  async deletesListTags(id) {
    const listTagsDelete = await this.listTagsRepository.update(
      { permanently_disable: true },
      {
        where: {
          id,
        },
      },
    );
    return listTagsDelete;
  }
}
