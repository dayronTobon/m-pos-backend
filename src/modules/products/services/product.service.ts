import { Inject, Injectable } from '@nestjs/common';
import { Product } from '../models/entity/product.entity';
import { configColumnsProduct } from './product-columns';
import { Category } from 'src/modules/categories/models/entity/categoty.entity';
import { QueryTypes } from 'sequelize';

@Injectable()
export class ProductService {
  constructor(
    @Inject('PRODUCT_REPOSITORY') private productRepository: typeof Product,
  ) {}

  async getListProducts() {
    // const [listProducts, count] = await this.productRepository.sequelize.query(
    //   `
    //     SELECT
    //       product.id,
    //       product.name,
    //       product.id_category,
    //       product.cost,
    //       product.price,
    //       product.status,
    //       product.permanently_disable,
    //       product.createdAt,
    //       product.updatedAt,
    //       category.id AS categoryId,
    //       category.name AS categoryName
    //       FROM products AS product
    //     LEFT OUTER JOIN
    //       Categories AS category
    //       ON product.id_category = category.id
    //     WHERE product.permanently_disable = 0;
    //   `,
    //   { logging: false },
    // );
    this.productRepository.associate(Category, 'category');
    const listProducts = await this.productRepository.findAll<Product>({
      // attributes: ['id', 'name', 'id_category'],
      include: [
        {
          model: Category,
          attributes: ['id', 'name'],
        },
      ],
      where: { permanently_disable: false },
      logging: false,
    });
    const res = {
      list: listProducts,
      configColumns: configColumnsProduct,
    };
    return res;
  }

  async getProduct(id: string) {
    return await this.productRepository.findByPk<Product>(id);
  }

  async createProduct(ProductData: any) {
    const dataInsert = await this.productRepository.build(ProductData);
    const product = await this.productRepository.create(dataInsert.dataValues, {
      logging: false,
    });
    return product;
  }

  async updateProduct(id, ProductData) {
    const productUpdate = await this.productRepository.update(ProductData, {
      where: {
        id,
      },
    });
    return productUpdate;
  }

  async changeStatusProduct(id, status: boolean) {
    const productUpdate = await this.productRepository.update(
      { status },
      {
        where: {
          id,
        },
      },
    );
    return productUpdate;
  }

  async deletesProduct(id) {
    const productDelete = await this.productRepository.update(
      { permanently_disable: true },
      {
        where: {
          id,
        },
      },
    );
    return productDelete;
  }
}
