import { Module } from '@nestjs/common';
import { ProductController } from './controllers/product.controller';
import { ListTagsController } from './controllers/list-tags.controller';
import { ProductService } from './services/product.service';
import { ListTagsService } from './services/list-tags.service';
import { productProviders } from './providers/product.providers';
import { MessageService } from '../../helpers/message.service';

@Module({
  controllers: [ProductController, ListTagsController],
  providers: [
    ...productProviders,
    ProductService,
    ListTagsService,
    MessageService,
  ],
})
export class ProductsModule {}
