import { ListTags } from '../models/entity/list-tags.entity';
import { Product } from '../models/entity/product.entity';

export const productProviders = [
  {
    provide: 'PRODUCT_REPOSITORY',
    useValue: Product,
  },
  {
    provide: 'LIST_TAGS_REPOSITORY',
    useValue: ListTags,
  },
];
