import {
  Table,
  Column,
  Model,
  HasMany,
  BelongsTo,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { IProduct } from '../interface/product.interface';
import { ListTags } from './list-tags.entity';
import { IListTags } from '../interface/list-tags.interface';
import { Category } from '../../../categories/models/entity/categoty.entity';
import { DataTypes, Sequelize } from 'sequelize';
import { configDB } from 'src/config/config-env';

@Table({
  tableName: 'products',
})
export class Product extends Model implements IProduct {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
  })
  id: string;

  @Column
  name: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  @ForeignKey(() => Category)
  @BelongsTo(() => Category, { as: 'category', foreignKey: 'id_category' })
  id_category: string;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  cost: number;

  @Column({
    type: DataType.INTEGER,
    allowNull: false,
  })
  price: number;

  @HasMany(() => ListTags)
  list_tags: IListTags[];

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  status?: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  permanently_disable?: boolean;

  static associate(models, nameModel) {
    // define association here
    switch (nameModel.toUpperCase()) {
      case 'CATEGORY':
        Product.belongsTo(models, {
          targetKey: 'id',
          foreignKey: 'id_category',
        });
        break;
      case 'LIST-TAGS':
        ListTags.hasMany(models, { foreignKey: 'id_product' });
        break;
      default:
        break;
    }
  }
}
