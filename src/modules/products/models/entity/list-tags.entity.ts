import {
  Table,
  Column,
  Model,
  BelongsTo,
  DataType,
  ForeignKey,
} from 'sequelize-typescript';
import { IListTags } from '../interface/list-tags.interface';
import { Product } from './product.entity';
import { IProduct } from '../interface/product.interface';

@Table({
  tableName: 'list_tags',
})
export class ListTags extends Model implements IListTags {
  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
    primaryKey: true,
  })
  id: string;

  @Column
  value: string;

  @Column
  description: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  @ForeignKey(() => Product)
  @BelongsTo(() => Product, { as: 'product', foreignKey: 'id_product' })
  id_product: string;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: true,
  })
  status?: boolean;

  @Column({
    type: DataType.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  })
  permanently_disable?: boolean;

  static associate(models, nameModel) {
    // define association here
    switch (nameModel.toUpperCase()) {
      case 'PRODUCT':
        ListTags.belongsTo(models, {
          targetKey: 'id',
          foreignKey: 'id_product',
        });
        break;
      default:
        break;
    }
  }
}
