import { IListTags } from './list-tags.interface';

export interface IProduct {
  name: string;
  id_category: string;
  cost: number;
  price: number;
  list_tags?: IListTags[];
  status?: boolean;
  permanently_disable?: boolean;
}
