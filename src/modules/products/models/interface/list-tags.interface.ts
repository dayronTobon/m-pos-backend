export interface IListTags {
  value: string;
  id_product: string;
  description: string;
  status?: boolean;
  permanently_disable?: boolean;
}
