'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface
      .changeColumn(
        'products', // name of Source model
        'id_category', // name of the key we're adding
        {
          type: Sequelize.UUID,
          references: {
            model: 'categories', // name of Target model
            key: 'id', // key in Target model that we're referencing
          },
          onDelete: 'CASCADE',
          onUpdate: 'CASCADE',
        },
      )
      .then(() =>
        queryInterface.changeColumn(
          'list_tags', // name of Source model
          'id_product', // name of the key we're adding
          {
            type: Sequelize.UUID,
            references: {
              model: 'products',
              key: 'id',
            },
            onDelete: 'CASCADE',
            onUpdate: 'CASCADE',
          },
        ),
      );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface
      .removeColumn('products', 'id_category')
      .then(() => queryInterface.removeColumn('list_tags', 'id_product'));
  },
};
