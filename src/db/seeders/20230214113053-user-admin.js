'use strict';
const bcrypt = require('bcrypt');
const { v4: uuidv4 } = require('uuid');
require('dotenv').config();

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const password = await bcrypt.hashSync(process.env.PSS, 10);
    await queryInterface.bulkInsert(
      'users',
      [
        {
          id: uuidv4(),
          full_name: 'MPOS Global Colomnbia',
          username: 'mpos',
          password: password,
          rol: 'ADMIN',
          status: true,
          permanently_disable: false,
          last_entry_date: new Date(),
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    );
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', { username: 'mpos' }, {});
  },
};
