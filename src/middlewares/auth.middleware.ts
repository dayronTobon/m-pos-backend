import { HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { MessageService } from '../helpers/message.service';
import * as jwt from 'jsonwebtoken';
import { DecodeOptions } from 'jsonwebtoken';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(private messageService: MessageService) {}

  async use(req: any, res: any, next: () => void) {
    if (!req.headers['authorization']) {
      const errorCreate = await this.messageService.messageOk(
        HttpStatus.FORBIDDEN,
        'user did not send the token',
      );
      res.status(HttpStatus.FORBIDDEN).json(errorCreate);
    } else {
      const API_SECRET = process.env.TOKEN_SECRET_KEY;
      jwt.verify(
        req.headers['authorization'].replace('Bearer ', ''),
        API_SECRET,
        async (err: Error, decoded: DecodeOptions) => {
          if (err) {
            const errorCreate = await this.messageService.messageOk(
              HttpStatus.UNAUTHORIZED,
              'unauthorized user',
            );
            res.status(HttpStatus.UNAUTHORIZED).json(errorCreate);
          } else {
            next();
          }
        },
      );
    }
  }
}
